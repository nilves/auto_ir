// MRT tv controller
// RTC pins 2 3 
//RTClibrary
// https://www.instructables.com/id/Setting-a-Real-Time-Clock-RTC-with-an-Arduino-Pro-/

// IR stuff https://github.com/cyborg5/IRLib2 
// https://learn.adafruit.com/using-an-infrared-library/hardware-needed
// https://learn.adafruit.com/using-an-infrared-library/sending-ir-codes


/*todo
 * add buttons to send IR commands manually => not done as mounting options limit button use
 */



#include "IRLibAll.h"

#include <Wire.h>
#include "RTClib.h"

//#define RTC_SCL 3
//#define RTC_SDA 2

//uncomment to use the compile time as estimate in case of unset time
//#define SET_RTC_COMPILE_TIME


//#define IR_RECIVER_PIN 2 // digital pins used for interrutps 0, 1, 2, 3, 7
#define IR_RECIVER_PIN 7
// pin 9 is timer1 output used to send the signal

#define CMD_BITS 20
#define CMD_POWER   0xc
#define CMD_SOURCE  0x38
#define CMD_UP      0x58
#define CMD_RIGHT   0x5B //1005B
#define CMD_DOWN    0x59
#define CMD_OK      0x5C

#define POWER_ON_DELAY 30000 //ms
#define CMD_DELAY 300 //ms
#define USB_SELECT_DELAY 3000 //ms


RTC_DS1307 rtc;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

//variables used to store the alarm times
// 7:30
#define NVRAM_START_TIME_STORED   0
#define NVRAM_START_ALARM_HOUR    1
#define NVRAM_START_ALARM_MINUTE  2
#define NVRAM_END_TIME_STORED     3
#define NVRAM_END_ALARM_HOUR      4
#define NVRAM_END_ALARM_MINUTE    5

#define NVRAM_TIME_STORED_TAG  0X55

byte startAlarmHour    = 7;
byte startAlarmMinute  = 30;
// 22:00
byte endAlarmHour      = 22;
byte endAlarmMinute    = 00;

//blink a led when time is not set correctly
bool correctTime = false;

//RXLED = 17; // The RX LED has a defined Arduino pin
//TXLED = 30
//LED_BUILTIN
const int ledPin =  30;// the number of the LED pin
int ledState = LOW;             // ledState used to set the LED
unsigned long previousMillis = 0;        // will store last time LED was updated
const long interval = 100;           // interval at which to blink (milliseconds)

//Create a receiver object to listen on an interrupt pin
IRrecvPCI myReceiver(IR_RECIVER_PIN);

//Create a decoder object 
IRdecode myDecoder; 

IRsend mySender;

void sendPowerUp(){
  Serial.print(F("Power up the tv and start video"));
  
  mySender.send(RC6,CMD_POWER, 20);
  mySender.send(RC6,CMD_POWER, 20);
  Serial.print(F("."));
  delay(POWER_ON_DELAY);

  mySender.send(RC6,CMD_SOURCE, 20);
  mySender.send(RC6,CMD_SOURCE, 20);
  Serial.print(F("."));
  delay(CMD_DELAY);

  // go to top of menu
  for (int i = 0; i < 10; i++)
  {
    mySender.send(RC6,CMD_UP, 20);
    mySender.send(RC6,CMD_UP, 20);
    Serial.print(F("."));
    delay(CMD_DELAY);
  }
  //select one down from top
  mySender.send(RC6,CMD_DOWN, 20);
  mySender.send(RC6,CMD_DOWN, 20);
  Serial.print(F("."));
  delay(CMD_DELAY);
  mySender.send(RC6,CMD_DOWN, 20);
  mySender.send(RC6,CMD_DOWN, 20);
  Serial.print(F("."));
  delay(CMD_DELAY);

  mySender.send(RC6,CMD_OK, 20);
  mySender.send(RC6,CMD_OK, 20);
  Serial.print(F("."));
  delay(USB_SELECT_DELAY);
  
  //right x2
  mySender.send(RC6,CMD_RIGHT, 20);
  mySender.send(RC6,CMD_RIGHT, 20);
  Serial.print(F("."));
  delay(CMD_DELAY);
  mySender.send(RC6,CMD_RIGHT, 20);
  mySender.send(RC6,CMD_RIGHT, 20);
  Serial.print(F("."));
  delay(CMD_DELAY);
  
  //down x4
  mySender.send(RC6,CMD_DOWN, 20);
  mySender.send(RC6,CMD_DOWN, 20);
  Serial.print(F("."));
  delay(CMD_DELAY);
  mySender.send(RC6,CMD_DOWN, 20);
  mySender.send(RC6,CMD_DOWN, 20);
  Serial.print(F("."));
  delay(CMD_DELAY);
  mySender.send(RC6,CMD_DOWN, 20);
  mySender.send(RC6,CMD_DOWN, 20);
  Serial.print(F("."));
  delay(CMD_DELAY);
  mySender.send(RC6,CMD_DOWN, 20);
  mySender.send(RC6,CMD_DOWN, 20);
  Serial.print(F("."));
  delay(CMD_DELAY);

  mySender.send(RC6,CMD_OK, 20);
  mySender.send(RC6,CMD_OK, 20);
  Serial.println(F("."));
  
  myReceiver.enableIRIn();      //Restart receiver
}

void sendPowerDown()
{
  Serial.print(F("Power power down TV"));
  mySender.send(RC6,CMD_POWER, 20);
  mySender.send(RC6,CMD_POWER, 20);
  Serial.println(F("."));

  myReceiver.enableIRIn();      //Restart receiver
}

void testIrSend()
{
  Serial.println(F("Test power up and down sequence"));
  sendPowerUp();
  delay(10000);
  sendPowerDown();
}
void printCurrentTime();

void printHelp()
{
  Serial.println(F("IR controller by Nigul Ilves 2019"));
  Serial.println(F("Current times and alarms"));
  printCurrentTime();
  printStartAlarmTime();
  printEndAlarmTime();
  Serial.println(F("prints recieved IR commands and sends IR commands on specified alarm times"));
  Serial.println(F("\th - prints this help"));
  Serial.println(F("\tu - Power up the tv and start video"));
  Serial.println(F("\td - Power power down TV"));
  Serial.println(F("\tx - Test power up and down sequence"));
  Serial.println(F("\tt - Set new clock time"));
  Serial.println(F("\ts - Set alarm time for start of tv"));
  Serial.println(F("\te - Set alarm time for end of tv"));
  Serial.println(F("\tm - dump the rtc nvram memory"));
}

void printTime(DateTime t)
{
  Serial.print(t.year(), DEC);
  Serial.print('/');
  Serial.print(t.month(), DEC);
  Serial.print('/');
  Serial.print(t.day(), DEC);
  Serial.print(" (");
  Serial.print(daysOfTheWeek[t.dayOfTheWeek()]);
  Serial.print(") ");
  Serial.print(t.hour(), DEC);
  Serial.print(':');
  Serial.print(t.minute(), DEC);
  Serial.print(':');
  Serial.print(t.second(), DEC);
  Serial.println();
}

void printStartAlarmTime()
{
  Serial.print(F("Start TV Alarm time\t"));
  Serial.print(startAlarmHour);
  Serial.print(':');
  Serial.print(startAlarmMinute);
  Serial.println();
}

void printEndAlarmTime()
{
  Serial.print(F("End TV Alarm time\t"));
  Serial.print(endAlarmHour);
  Serial.print(':');
  Serial.print(endAlarmMinute);
  Serial.println();
}

void dumpRtcNvramMemory()
{
  Serial.println("Current RTC NVRAM values:");
  for (int i = 0; i < 56; ++i) {
    Serial.print("Address 0x");
    Serial.print(i, HEX);
    Serial.print(" = 0x");
    Serial.println(rtc.readnvram(i), HEX); 
  }
}

void storeStartAlarmTime(byte h, byte m)
{
    startAlarmHour    = h;
    rtc.writenvram(NVRAM_START_ALARM_HOUR, startAlarmHour);

    startAlarmMinute  = m;
    rtc.writenvram(NVRAM_START_ALARM_MINUTE, startAlarmMinute);
    
    rtc.writenvram(NVRAM_START_TIME_STORED, NVRAM_TIME_STORED_TAG);
}

void storeEndAlarmTime(byte h, byte m)
{
    endAlarmHour      = h;
    rtc.writenvram(NVRAM_END_ALARM_HOUR, endAlarmHour);
    
    endAlarmMinute    = m;
    rtc.writenvram(NVRAM_END_ALARM_MINUTE, endAlarmMinute);

    rtc.writenvram(NVRAM_END_TIME_STORED, NVRAM_TIME_STORED_TAG);
}

void loadStartAlarmTime()
{
    startAlarmHour    = rtc.readnvram(NVRAM_START_ALARM_HOUR);
    startAlarmMinute  = rtc.readnvram(NVRAM_START_ALARM_MINUTE);
}

void loadEndAlarmTime()
{
    endAlarmHour      = rtc.readnvram(NVRAM_END_ALARM_HOUR);
    endAlarmMinute    = rtc.readnvram(NVRAM_END_ALARM_MINUTE);
}


void initiateAlarmVariables()
{
  //debug
  //Serial.println(F("nvram memory before initiation"));
  //dumpRtcNvramMemory();
  
  if (rtc.readnvram(NVRAM_START_TIME_STORED) != NVRAM_TIME_STORED_TAG){
    Serial.println(F("Initiate start alarm time to 7:30"));
    storeStartAlarmTime(7, 30);
  } else{
    Serial.println(F("load the stored start alarm time"));
    loadStartAlarmTime();
    printStartAlarmTime();
  }
  
  if (rtc.readnvram(NVRAM_END_TIME_STORED) != NVRAM_TIME_STORED_TAG)
  {
    Serial.println(F("Initiate end alarm time to 22:00"));
    storeEndAlarmTime(22, 00);
  }
  else
  {
    Serial.println(F("load the stored end alarm time"));
    loadEndAlarmTime();
    printEndAlarmTime();
  }

  //debug
  //Serial.println(F("nvram memory after initiation"));
  //dumpRtcNvramMemory();
}

void printCurrentTime()
{
  DateTime now = rtc.now();
  printTime(now);
}


//used to flush the new line char 10
void serialInFlush()
{
  while (Serial.available())
  {
    Serial.read();
  }
}

//https://www.arduino.cc/en/Tutorial.StringToIntExample
String inString = "";
//returns a negative value for invalid data input 
int readPosInteger()
{
  int returnValue = -1; //will be returned when could ont get data
  bool numbersRead = false;
  while (! Serial.available())
  {
    delay(10);
  }

  int inChar = Serial.read();
  Serial.print((char)inChar);
    
  while (isDigit(inChar))
  {
    // convert the incoming byte to a char and add it to the string:
    inString += (char)inChar;
    numbersRead = true;

    while (! Serial.available())
    {
      delay(10);
    }
    inChar = Serial.read();
    Serial.print((char)inChar);
  }
  
  if ((inChar == '\n') && (numbersRead == true))
  {    
    returnValue =  inString.toInt();
  }

  inString = "";
  return returnValue;
}


//https://www.instructables.com/id/Setting-the-DS1307-Real-Time-Clock-using-the-Seria/
void setTime()
{
  int newYear;
  int newMonth;
  int newDay;
  int newHour;
  int newMinute;
  int newSecond = 0;

  serialInFlush();

  //year
  while (1) {
    Serial.println(F("Please enter current year, 0-99"));
    Serial.print(F(">"));
    newYear = readPosInteger();
    if (newYear < 0) {
      Serial.println(F("Invalid character, try again"));
      continue;
    } else if (newYear > 99) {
      Serial.println(F("Too large number, try again"));
      continue;
    } else {
      break;
    }
  }

  newYear += 2000;

  //month
  while (1){
    Serial.println(F("Please enter current month, 1-12"));
    Serial.print(F(">"));
    newMonth = readPosInteger();
    if (newMonth < 0) {
      Serial.println(F("Invalid character, try again"));
      continue;
    } else if (newMonth < 1) {
      Serial.println(F("Too low number, try again"));
      continue;
    } else if (newMonth > 12) {
      Serial.println(F("Too large number, try again"));
      continue;
    } else {
      break;
    }
  }

  //day
  while (1){
    Serial.println(F("Please enter current day, 1-31"));
    Serial.print(F(">"));
    newDay = readPosInteger();
    if (newDay < 0) {
      Serial.println(F("Invalid character, try again"));
      continue;
    } else if (newDay < 1) {
      Serial.println(F("Too low number, try again"));
      continue;
    } else if (newDay > 31) {
      Serial.println(F("Too large number, try again"));
      continue;
    } else {
      break;
    }
  }
  
  //hour
  while (1){
    Serial.println(F("Please enter current hour in 24hr format, 0-23"));
    Serial.print(F(">"));
    newHour = readPosInteger();
    if (newHour < 0) {
      Serial.println(F("Invalid character, try again"));
      continue;
    } else if (newHour < 0) {
      Serial.println(F("Too low number, try again"));
      continue;
    } else if (newHour > 23) {
      Serial.println(F("Too large number, try again"));
      continue;
    } else {
      break;
    }
  }

  //minute
  while (1){
    Serial.println(F("Please enter the current minute, 0-59"));
    Serial.print(F(">"));
    newMinute = readPosInteger();
    if (newMinute < 0) {
      Serial.println(F("Invalid character, try again"));
      continue;
    } else if (newMinute < 0) {
      Serial.println(F("Too low number, try again"));
      continue;
    } else if (newMinute > 59) {
      Serial.println(F("Too large number, try again"));
      continue;
    } else {
      break;
    }
  }

  DateTime newTime = DateTime(newYear, newMonth, newDay, newHour, newMinute, newSecond);
  //Serial.println(F("You have input following time:"));
  //printTime(newTime);

  rtc.adjust(newTime);

  correctTime = true;
  
  Serial.println(F("Done updating the time. Current time is now"));
  printCurrentTime();
}


void setStartAlarmTime()
{
  Serial.println(F("Updating the Start alarm time for tv"));

  byte h = 0; //startAlarmHour
  byte m = 0; //startAlarmMinute
  serialInFlush();

  //hour
  while (1){
    Serial.println(F("Please enter hour in 24hr format, 0-23"));
    Serial.print(F(">"));
    h = readPosInteger();
    if (h < 0) {
      Serial.println(F("Invalid character, try again"));
      continue;
    } else if (h < 0) {
      Serial.println(F("Too low number, try again"));
      continue;
    } else if (h > 23) {
      Serial.println(F("Too large number, try again"));
      continue;
    } else {
      break;
    }
  }

  //minute
  while (1){
    Serial.println(F("Please enter minute, 0-59"));
    Serial.print(F(">"));
    m = readPosInteger();
    if (m < 0) {
      Serial.println(F("Invalid character, try again"));
      continue;
    } else if (m < 0) {
      Serial.println(F("Too low number, try again"));
      continue;
    } else if (m > 59) {
      Serial.println(F("Too large number, try again"));
      continue;
    } else {
      break;
    }
  }

  storeStartAlarmTime(h, m);
  
  Serial.println(F("Done updating the start tv alarm time"));
  printStartAlarmTime();
}

void setEndAlarmTime()
{
  Serial.println(F("Updating the end alarm time for tv"));

  byte h = 0; //endAlarmHour
  byte m = 0; //endAlarmMinute

  serialInFlush();

  //hour
  while (1){
    Serial.println(F("Please enter hour in 24hr format, 0-23"));
    Serial.print(F(">"));
    h = readPosInteger();
    if (h < 0) {
      Serial.println(F("Invalid character, try again"));
      continue;
    } else if (h < 0) {
      Serial.println(F("Too low number, try again"));
      continue;
    } else if (h > 23) {
      Serial.println(F("Too large number, try again"));
      continue;
    } else {
      break;
    }
  }

  //minute
  while (1){
    Serial.println(F("Please enter minute, 0-59"));
    Serial.print(F(">"));
    m = readPosInteger();
    if (m < 0) {
      Serial.println(F("Invalid character, try again"));
      continue;
    } else if (m < 0) {
      Serial.println(F("Too low number, try again"));
      continue;
    } else if (m > 59) {
      Serial.println(F("Too large number, try again"));
      continue;
    } else {
      break;
    }
  }

  storeEndAlarmTime(h, m);
  
  Serial.println(F("Done updating the end tv alarm time"));
  printEndAlarmTime();
}

bool startAlarmActive = false;
bool endAlarmActive   = false;

void checkAlarmsSendIrCommands()
{
  DateTime t = rtc.now();
  
  if ( (t.hour() == startAlarmHour) && (t.minute() == startAlarmMinute) )
  {
    // send command only one time
    if (startAlarmActive == false)
    {
      Serial.println(F("Start alarm active!"));
      Serial.print(F(">"));
      startAlarmActive = true;
      sendPowerUp();    
    }      
  }
  else
  {
    startAlarmActive = false;
  }

  if ( (t.hour() == endAlarmHour) && (t.minute() == endAlarmMinute) )
  {
    // send command only one time
    if (endAlarmActive == false)
    {
      Serial.println(F("Start alarm active!"));
      Serial.print(F(">"));
      endAlarmActive = true;
      sendPowerDown();    
    }      
  }
  else
  {
    endAlarmActive = false;
  }
  
}

void blinkTheLed()
{
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(ledPin, ledState);
  }
}

void setup() {
  Serial.begin(9600);
  delay(2000); while (!Serial); //delay for Leonardo

  Serial.print(F("IR controller by Nigul Ilves 2019: "));
  Serial.println(F("Starting..."));

  pinMode(ledPin, OUTPUT);

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running! **UPDATE THE TIME MANUALLY** witt \"t\" command");

#ifdef SET_RTC_COMPILE_TIME    
    Serial.println("Setting time to best estimate (compile time):");
    
    // following line sets the RTC to the date & time this sketch was compiled

    DateTime setTime = DateTime(F(__DATE__), F(__TIME__));
    printTime(setTime);
    
    rtc.adjust(setTime);

    correctTime = true;
#else
    // note that wrong time is used
    correctTime = false;
#endif

  }
  else 
  {
    correctTime = true;
  }

  //load or initiate the alarm times for sending IR commands  
  startAlarmActive = false;
  endAlarmActive   = false;
  initiateAlarmVariables();
  
  myReceiver.enableIRIn(); // Start the receiver
  printHelp();
  Serial.print(F(">"));
}

void loop() {
  if (myReceiver.getResults()) {
    myDecoder.decode();           //Decode it
    myDecoder.dumpResults(false);  //Now print results. Use false for less detail
    myReceiver.enableIRIn();      //Restart receiver
    Serial.print(F(">"));
  }

  //printCurrentTime();

  if (correctTime == false)
  {
    blinkTheLed();
  }
  
  //Most important => do the work
  checkAlarmsSendIrCommands();

  if (Serial.available() > 0) {
    int inByte = Serial.read();
    // do something different depending on the character received.
    // The switch statement expects single number values for each case; in this
    // example, though, you're using single quotes to tell the controller to get
    // the ASCII value for the character. For example 'a' = 97, 'b' = 98,
    // and so forth:

    if(inByte != 10)
    {
      Serial.println((char)inByte);
      
      switch (inByte) {
        case 'u':
          sendPowerUp();
          break;
        case 'd':
          sendPowerDown();
          break;
        case 'x':
          testIrSend();
          break;
        case 'h':
          printHelp();
          break;
        case 't':
          setTime();
          break;
        case 's':
          setStartAlarmTime();
          break;
        case 'e':
          setEndAlarmTime();
          break;
        case 'm':
          dumpRtcNvramMemory();
          break;
        default:
          Serial.println(F("Unknown command!"));
          printHelp();
      }
  
      Serial.print(F(">"));
    }
  }
}

/*
 * test
    mySender.send(SONY,0x6D25, 15);
    delay(500);
    mySender.send(SONY,0xAF, 12);
    delay(500);
    mySender.send(SONY,0xAF, 12);
    delay(500);
    mySender.send(SONY,0x62E9, 15);
    delay(500);
*/
